<?php
/*
 * Provides administration overview page for metatags by path settings
 */
function metatag_context_path_config_overview() {
  $contexts = context_enabled_contexts(TRUE);
  $header = array(t('Name'), t('Paths'), t('Operations'));
  $rows = array();

  foreach ($contexts as $name => $context) {
    if (isset($context->reactions['metatag_context_reaction']['metatag_admin']) &&  $context->reactions['metatag_context_reaction']['metatag_admin'] && isset($context->conditions['path']['values'])) {
      $ops = array(
        l('Edit', 'admin/config/search/metatags/path_config/'.$context->name, array('query' => array('destination' => 'admin/config/search/metatags/path_config'))),
        l('Delete', 'admin/config/search/metatags/path_config/'.$context->name . '/delete', array('query' => array('destination' => 'admin/config/search/metatags/path_config'))),
      );
      $rows[] = array(
          $context->name,
          implode(', ', $context->conditions['path']['values']),
          implode(' | ', $ops),
      );
    }
  }
  return theme('table', array('header' => $header, 'rows' => $rows));
}

function metatag_context_config_add_form($form, &$form_state) {
  $form['name'] = array(
    '#title' => 'Name',
    '#type' => 'textfield',
    '#default_value' => '',
    '#description' => 'The unique ID for this metatag path context rule. This must contain only lower case letters, numbers and underscores.',
    '#required' => 1,
    '#maxlength' => 255,
    '#element_validate' => Array('metatag_context_edit_name_validate'),
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Add and configure'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/search/metatags/path_config',
  );
  return $form;
}

function metatag_context_edit_name_validate($element, &$form_state) {
  // Check for string identifier sanity
  if (!preg_match('!^[a-z0-9_-]+$!', $element['#value'])) {
    form_error($element, t('The name can only consist of lowercase letters, underscores, dashes, and numbers.'));
    return;
  }
  // Check for name collision
  if ($exists = ctools_export_crud_load('context', $element['#value'])) {
    form_error($element, t('A context with this name already exists. Please choose another name or delete the existing item before creating a new one.'));
  }
}

function metatag_context_config_add_form_submit($form, &$form_state) {
  $context = metatag_context_load_default_context();
  $context->name = $form_state['values']['name'];
  context_save($context);
  $form_state['redirect'] = 'admin/config/search/metatags/path_config/'.$context->name;
}

function metatag_context_config_edit_form($form, &$form_state, $context) {
  $form_state['metatag_context']['context'] = $context;
  // Empty form to start with.
  $form = array();
  // Don't care about the instance name, the data is being managed by
  // Context and not Metatag.
  $instance = "";
  $options = array();
  // Load the METATAG form.
  metatag_metatags_form($form, $instance, $context->reactions['metatag_context_reaction']['metatags'], $options);

  $form['paths'] = array(
    '#title' => 'Path',
    '#description' => 'Set this metatag pat context when any of the paths above match the page path. Put each path on a separate line. You can use the <code>*</code> character (asterisk) as a wildcard and the <code>~</code> character (tilde) to exclude one or more paths. Use &lt;front&gt; for the site front page.',
    '#type' => 'textarea',
    '#default_value' => isset($context->conditions['path']['values']) ? html_entity_decode(implode('&#13;&#10;', $context->conditions['path']['values'])) : '',
    '#required' => 1,
    '#weight' => -100,
  );
  //Show all tokens
  $form['metatags']['tokens']['#token_types'] = 'all';

  $form['metatags']['#type'] = 'container';
  unset($form['metatags']['#collapsed']);
  unset($form['metatags']['#collapsible']);

  $form['actions']['#type'] = 'actions';
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => array('metatag_context_config_edit_form_cancel_submit'),
    '#limit_validation_errors' => array(),
  );
  $form['#submit'][] = 'metatag_context_config_edit_form_submit';

  return $form;
}

function metatag_context_config_edit_form_cancel_submit($form, &$form_state) {
  context_delete($form_state['metatag_context']['context']);
  $form_state['redirect'] = 'admin/config/search/metatags/path_config';
}

function metatag_context_config_edit_form_submit($form, &$form_state) {
  //print_r($form_state); die;
  $context = $form_state['metatag_context']['context'];
  $context->reactions['metatag_context_reaction']['metatags'] = array_merge($context->reactions['metatag_context_reaction']['metatags'], $form_state['values']['metatags']);
  $paths = explode("\n", str_replace("\r", "", $form_state['values']['paths']));
  $paths = array_combine($paths, $paths);
  $context->conditions['path']['values'] = $paths;
  context_save($context);
  $form_state['redirect'] = 'admin/config/search/metatags/path_config';
}

function metatag_context_delete_form($form, &$form_state, $context) {
  $form_state['metatag_context']['context'] = $context;

  $form['delete'] = array(
    '#value' => 'This action will permanently remove this item from your database.'
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/search/metatags/path_config',
  );
  $form['#submit'][] = 'metatag_context_delete_form_submit';
  return $form;
}

function metatag_context_delete_form_submit($form, &$form_state) {
  context_delete($form_state['metatag_context']['context']);
  $form_state['redirect'] = 'admin/config/search/metatags/path_config';
}

function metatag_context_load_default_context() {
  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'default_metatag_context';
  $context->description = '';
  $context->tag = 'Metatag';
  $context->metatag = TRUE;
  $context->conditions = array(
    'path' => array(
      'values' => array(
      ),
    ),
  );
  $context->reactions = array(
    'metatag_context_reaction' => array(
      'metatags' => array(
        'description' => array(
          'value' => '',
        ),
        'keywords' => array(
          'value' => '',
        ),
        'title' => array(
          'value' => '[current-page:title] | [site:name]',
          'default' => '[current-page:title] | [site:name]',
        ),
      ),
      'metatag_admin' => 1,
    ),
  );
  $context->condition_mode = 0;
  $context->weight = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Metatag');
  return $context;
}
