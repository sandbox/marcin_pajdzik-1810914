The purpose of this module is to closely integrate Context (http://drupal.org/project/context) 
and Meta Tags (http://drupal.org/project/metatag) modules. 
The module lavareges both modules making it possible to set metatags on any drupal path. 

It also provides:

- an interface that integrates with the Metatag module 
- a support for tokens.

This module is based on the Context Metadata (http://drupal.org/project/context_metadata) 
module and aims at solving the following issues:
 
#1187710: Integrate with Context (http://drupal.org/node/1187710)
#1160340: Rewrite as a submodule of Metatags (http://drupal.org/node/1160340)

Ideally this project will become a submodule of the Meta Tags module once it is ready for full release.

<strong>Notes</strong>

* the interface is available at admin/config/search/metatags/path_config
* the newest Meta Tags dev realease should be used with this module

The integration work was sponsored by <a href="http://www.dennis.co.uk">Dennis Publishing</a>